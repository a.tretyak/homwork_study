const input = document.getElementById("price");
const title = document.getElementById("title");
const clear = document.getElementById("clear");
const error = document.getElementById("error");

input.addEventListener("focus", () => {
    title.style.display = "none";
    clear.style.display = "none";
    input.style.borderColor = "";
});

input.addEventListener("blur", ({ target }) => {
    const price = Number.parseInt(target.value);

    if (isNaN(price) || price <= 0) {
        target.value = 0;

        error.style.display = "block";
        input.style.borderColor = "red";

        return;
    }

    title.style.display = "block";
    clear.style.display = "block";
    error.style.display = "none";

    title.textContent = `Текущая цена: ${price} `;
    target.value = price;
});

clear.addEventListener("click", () => {
    title.style.display = "none";
    clear.style.display = "none";
    input.value = 0;
});