let number1 = +prompt ('input number1');
let number2 = +prompt('input number2');
let operation = prompt('input operation');

function calculate (a, b, operation) {
  switch (operation) {
    case '+':
      return a + b;

    case '-':
      return a - b;

    case '/':
      return a / b;

    case '*':
      return a * b;
  }
}

const result = calculate (number1, number2, operation);

console.log(result);
